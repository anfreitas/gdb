from enum import Enum
from requests_futures.sessions import FuturesSession
import logging
import json
from datetime import datetime
from pydq import TIME_FORMAT
from queue import Empty
import os


class Status(Enum):
    OK = 'ok'
    ERROR = 'error'
    WARN = 'warn'
    ALERT = 'alert'
    UNKNOWN = 'unknown'

    @property
    def qicon(self):
        from PyQt5.QtGui import QIcon
        return QIcon(self.icon_path)

    @property
    def icon_path(self):
        from fbs_runtime.application_context.PyQt5 import ApplicationContext
        return ApplicationContext().get_resource('status/%s.png' % self.value)


class SQSHandler(logging.Handler):
    def __init__(self):
        logging.Handler.__init__(self)
        self.setFormatter(logging.Formatter(CloudLogger.LOG_FORMAT))
        self.dq_provider = None
        self.gid = None
        self.url = None
        self.api_key = None
        self.session = FuturesSession(max_workers=10)

    def emit(self, record):
        if self.dq_provider is None:
            return
        with self.dq_provider('__system__') as dq:
            try:
                if self.gid is None:
                    dq('gid', limit=1)
                    self.gid = dq.get(block=False)['val']
                if self.url is None:
                    dq('log_url', limit=1)
                    self.url = dq.get(block=False)['val']
                if self.api_key is None:
                    dq('log_api_key', limit=1)
                    self.api_key = dq.get(block=False)['val']
                record.name = self.gid
                self.session.post(self.url + "/logs", json=json.dumps(record.__dict__),
                                  headers={"x-api-key": self.api_key, "Content-Type": "application/json"},
                                  timeout=(3.05, 15))
            except Exception as e:
                pass


class CloudLogger(logging.Logger):
    LOG_FORMAT = '%(asctime)s - %(levelname)s: %(message)s'
    LOG_LEVEL = logging.INFO

    def __init__(self, name, log_file=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'gdb.log')):
        super().__init__(name, CloudLogger.LOG_LEVEL)
        self.log_file = log_file
        if not os.path.exists(os.path.dirname(self.log_file)):
            os.makedirs(os.path.dirname(self.log_file))
        file_handler = logging.FileHandler(self.log_file, mode='w')
        file_handler.setFormatter(logging.Formatter(CloudLogger.LOG_FORMAT))
        self.addHandler(file_handler)
        self.sqs_handler = SQSHandler()
        self.addHandler(self.sqs_handler)
        self.dq_provider = None

    def set_dq_provider(self, dq_provider):
        self.dq_provider = dq_provider
        with dq_provider('__system__') as dq:
            try:
                dq('log_url', limit=1)
                dq.get(block=False)
            except Empty:
                dq.put({'qid': 'log_url', 'ts': datetime.strftime(datetime.utcnow(), TIME_FORMAT), 'val': ''})
            try:
                dq('log_api_key', limit=1)
                dq.get(block=False)
            except Empty:
                dq.put({'qid': 'log_api_key', 'ts': datetime.strftime(datetime.utcnow(), TIME_FORMAT), 'val': ''})
        self.sqs_handler.dq_provider = self.dq_provider
