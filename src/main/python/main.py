import os
import sys
import logging
from time import sleep
from symbols import CloudLogger

logging.setLoggerClass(CloudLogger)


if hasattr(sys, 'frozen'):
    os.environ['PATH'] = sys._MEIPASS + os.pathsep + os.environ['PATH']


from fbs_runtime.application_context.PyQt5 import ApplicationContext
from client import Client
from agent import Agent
from api import init_api
from importlib import import_module
from threading import Thread
from argparse import ArgumentParser

from pydq.local import SQLite, TIME_FORMAT
from queue import Empty
from datetime import datetime
from gdbcore.broker.aadi import AADIBroker
from gdbcore.broker.akva import AKVABroker
from gdbcore.broker.gctide import GCTideBroker
from gdbcore.broker.cleaner import Cleaner
from gdbcore.listener.gdbshare import GDBShare
from gdbcore.listener.eagleio import EagleioListener

try:
    from gdbcore.broker.test import RandomFloat
except:
    pass


def get_class_from_string(name):
    split = name.split('.')
    module = '.'.join(split[:-1])
    class_name = split[-1]
    module = import_module(module)
    return getattr(module, class_name)


if __name__ == '__main__':
    appctxt = ApplicationContext()
    args = ArgumentParser()
    args.add_argument('--headless', action="store_true", default=False)
    args.add_argument('-g', '--gid')
    args = args.parse_args()
    dq_provider = SQLite
    logging.getLogger('GDB').set_dq_provider(dq_provider)
    try:
        with dq_provider('__system__') as dq:
            gid = dq('gid').get()['val']
    except Empty:
        logging.getLogger('GDB').info('No gid set. Retrieving from args')
        gid = args.gid
        with dq_provider('__system__') as dq:
            dq.put({'qid': 'gid', 'ts': datetime.utcnow().strftime(TIME_FORMAT), 'val': gid})
    brokers = [AADIBroker(dq_provider), AKVABroker(dq_provider), GCTideBroker(dq_provider), Cleaner(dq_provider)]
    listeners = [GDBShare(dq_provider), EagleioListener(dq_provider)]
    if gid == 'Testing':
        brokers.insert(0, RandomFloat(dq_provider))
    for broker in brokers[:-1]:     # all but cleaner
        [broker.register_listener(listener) for listener in listeners]
    agent = Agent(brokers)
    agent.start()
    api = init_api(dq_provider, agent, appctxt)
    if args.headless:
        api.start_local()
    else:
        api_thread = Thread(target=api.start_local)
        api_thread.start()
        sleep(1)    # give the api a chance to start
        client = Client(dq_provider, agent, api)
        exit_code = appctxt.app.exec_()
        sys.exit(exit_code)
