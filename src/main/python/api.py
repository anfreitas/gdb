from gdbapi import init_lambda_page


def run_all_brokers(event):
    agent = event['context']['agent']
    for name in sorted(agent.broker_runners.keys()):    # Share should be last
        agent.broker_runners[name].run()
    return 200, {name: broker.status.value.lower() for name, broker in agent.broker_runners.items()}


def run_broker(event):
    agent = event['context']['agent']
    bname = event['queryStringParameters']['bname']
    agent.broker_runners[bname].run()
    return 200, str(agent.broker_runners[bname].status.value.lower())


def get_broker_runner_status(event):
    agent = event['context']['agent']
    bname = event['queryStringParameters']['bname']
    return 200, agent.broker_runners[bname].status.value.lower()


def _get_resource_pyqt(name, event):
    appctxt = event['context']['appctxt']
    with open(appctxt.get_resource(name)) as f:
        return f.read()


def init_api(dq_provider, agent=None, appctxt=None):
    page = init_lambda_page(dq_provider)
    page.context['agent'] = agent
    page.context['appctxt'] = appctxt
    page.context['_get_resource'] = _get_resource_pyqt
    page.add_endpoint('get', '/run', run_broker, 'application/json')
    page.add_endpoint('get', '/runAll', run_all_brokers, 'application/json')
    page.add_endpoint('get', '/status', get_broker_runner_status, 'application/json')
    return page


if __name__ == '__main__':
    from pydq.local import SQLite
    api = init_api(SQLite)
    api.start_local()
