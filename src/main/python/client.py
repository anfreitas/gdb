from symbols import Status

from PyQt5.QtWidgets import QMainWindow, QAction, QMenu, qApp, QSystemTrayIcon
from PyQt5.QtCore import QUrl, Qt


class Client(QMainWindow):
    def __init__(self, data_queue_provider, agent, api):
        super().__init__()
        self.data_queue_provider = data_queue_provider
        self.agent = agent
        self.api = api
        self.tray_icon = QSystemTrayIcon(self)
        self.build_ui()

    def build_ui(self):
        self.set_tray_icon()
        quit_action = QAction("Exit gdb", self)
        quit_action.triggered.connect(self.quit)
        tray_menu = QMenu()
        tray_menu.addAction(quit_action)
        self.tray_icon.setContextMenu(tray_menu)
        self.tray_icon.show()

    def closeEvent(self, event):
        event.ignore()
        self.hide()
        self.tray_icon.showMessage(
            "gdb",
            "gdb was minimized to tray",
            QSystemTrayIcon.Information,
            2000
        )

    def set_tray_icon(self):
        for broker_runner in self.agent.broker_runners.values():
            if broker_runner.status != Status.OK:
                self.tray_icon.setIcon(Status.ALERT.qicon)
                return
        self.tray_icon.setIcon(Status.OK.qicon)

    def quit(self):
        self.api.httpd.shutdown()
        self.agent.running = False
        self.agent.join()
        qApp.quit()
