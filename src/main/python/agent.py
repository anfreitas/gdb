from time import time, sleep
from threading import Thread
import logging
from traceback import print_exc

from PyQt5.QtCore import pyqtSignal, QThread

from symbols import Status


class Agent(Thread):
    def __init__(self, brokers):
        super().__init__()
        self.running = True
        self.broker_runners = {broker.NAME: BrokerRunner(broker) for broker in brokers}

    def run(self):
        while self.running:
            t = time()
            for broker_runner in self.broker_runners.values():
                if t - broker_runner.counter > broker_runner.broker.interval:
                    broker_runner.start()
                    broker_runner.counter = t
            sleep(2)


class BrokerRunner(QThread):
    tab_update = pyqtSignal(QThread)

    def __init__(self, broker):
        super().__init__()
        self.broker = broker
        self.counter = 0
        self._status = Status.UNKNOWN
        self.status_detail = 'Unknown'
        self.logger = logging.getLogger('GDB')

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, status):
        self._status = status
        self.tab_update.emit(self)

    def run(self):
        try:
            with self.broker.config() as config:
                if not bool(config['enabled']):
                    self.logger.info('Skipping %s run' % self.broker.NAME)
                    return
            self.logger.info('Starting %s run' % self.broker.NAME)
            self.broker.run()
            self.status_detail = 'OK'
            self.status = Status.OK
        except Exception as e:
            print_exc()
            self.status_detail = str(e)
            self.status = Status.ERROR
        self.logger.info('Finished %s run with %s: %s' % (self.broker.NAME, str(self.status), self.status_detail))
